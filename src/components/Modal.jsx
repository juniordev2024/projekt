import { useEffect, useState } from "react";
import axios from "axios";

const Modal = ({ imeRadionice, setModal, idRadionice, brojPrijava }) => {
  const [thanksState, setThanksState] = useState(false);
  const [formaPodaci, setFormaPodaci] = useState({
    imePrezime: "",
    email: "",
    razlogPrijave: "",
  });

  function promjenaPolja(e) {
    const { name, value } = e.target;
    setFormaPodaci({ ...formaPodaci, [name]: value });
  }

  function handleSubmit() {
    saljiZahtjev();
    let emailRegex = /^(?!\s).+@.+(?<!\s)$/;

    if (formaPodaci.imePrezime.length == 0) {
      alert("Unesite Vaše ime");
      return;
    }
    if (formaPodaci.email.length == 0) {
      alert("Unesite Vaš e-mail");
      return;
    }
    if (!emailRegex.test(formaPodaci.email)) {
      alert("Unesite potpunu e-mail adresu");
      return;
    }
    if (formaPodaci.razlogPrijave.length == 0) {
      alert("Unesite Vaš razlog prijave");
      return;
    }
    setThanksState(true);
  }

  function saljiZahtjev() {
    axios.patch(`http://localhost:3001/radionice/${idRadionice}`, {
      broj_prijava: brojPrijava + 1,
    });
  }

  return (
    <div className="modalContainer">
      {!thanksState ? (
        <form className="modalPrijava">
          <span
            className="close"
            onClick={() => {
              setModal(false);
            }}
          >
            &times;
          </span>
          <h2 style={{ paddingTop: "50px" }}>Prijavi se na {imeRadionice}</h2>
          <div style={{ padding: "10px" }}>
            <input
              type="text"
              name="imePrezime"
              required
              placeholder="Puno ime i prezime"
              value={formaPodaci.imePrezime}
              onChange={promjenaPolja}
            ></input>
            <br />
            <input
              type="email"
              name="email"
              required
              placeholder="Email"
              value={formaPodaci.email}
              onChange={promjenaPolja}
            ></input>
            <br />
            <input
              type="text"
              name="razlogPrijave"
              required
              placeholder="Razlog prijave"
              onChange={promjenaPolja}
              value={formaPodaci.razlogPrijave}
            ></input>
            <br />
            <button
              className="botunPrijava"
              onClick={handleSubmit}
              style={{ marginTop: "20px" }}
            >
              Prijavi se
            </button>
          </div>
        </form>
      ) : (
        <form className="modalPrijava">
          <span
            className="close"
            onClick={() => {
              setModal(false);
              window.location.reload();
            }}
          >
            &times;
          </span>
          <h2 style={{ paddingTop: "50px" }}>Hvala na prijavi</h2>
          <div style={{ padding: "10px" }}>
            <p style={{ margin: "15px" }}>
              Vaša prijava je uspješno zaprimljena! Informacije o mjestu
              održavanja, vremenu i grupi koja će Vam biti dodijeljena biti će
              dostavljene na navedenu e-mail adresu. Za sva ostala pitanja
              obratite nam se na Edit@Code.com.
            </p>
            <button className="botunNatrag">Natrag na radionice</button>
          </div>
        </form>
      )}
    </div>
  );
};
export default Modal;
