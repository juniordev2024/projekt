import { useState, useEffect } from "react";
import axios from "axios";

const ModalData = ({
  dataType,
  ime = "",
  datum = "",
  predavac = "",
  opis = "",
  tema = [],
  tezina = "",
  slika = "",
  id,
  broj_prijava = 0,
  setShowModal,
  biografija = "",
  organizacija = "",
  radionice = [],
  refreshData,
  dodaj,
}) => {
  const [formData, setFormData] = useState({
    id: id,
    ime: ime,
    datum: datum,
    predavac: predavac,
    opis: opis,
    broj_prijava: broj_prijava,
    tema: tema,
    tezina: tezina,
    slika: slika,
    biografija: biografija,
    organizacija: organizacija,
    radionice: radionice,
  });
  const [predavaci, setPredavaci] = useState([]);
  const [radioniceSve, setRadioniceSve] = useState([]);
  const [teme, setTeme] = useState([]);
  const [tezine, setTezine] = useState([]);
  const [feedback, setFeedback] = useState("");

  useEffect(() => {
    axios
      .get("http://localhost:3001/predavaci")
      .then((res) => setPredavaci(res.data))
      .catch((error) => {
        console.error("Error fetching predavaci:", error);
      });

    axios
      .get("http://localhost:3001/teme")
      .then((res) => setTeme(res.data))
      .catch((error) => {
        console.error("Error fetching teme:", error);
      });

    axios
      .get("http://localhost:3001/tezine")
      .then((res) => setTezine(res.data))
      .catch((error) => {
        console.error("Error fetching tezine:", error);
      });

    axios
      .get("http://localhost:3001/radionice")
      .then((res) => setRadioniceSve(res.data))
      .catch((error) => {
        console.error("Error fetching sve radionice:", error);
      });
  }, []);

  useEffect(() => {
    if (dodaj) {
      setFormData({
        ime: "",
        datum: "",
        predavac: predavac || "Martin Marić",
        opis: "",
        broj_prijava: 0,
        tema: tema || "React",
        tezina: tezina || "Junior",
        slika: "",
        biografija: "",
        organizacija: "",
        radionice: [],
      });
    }
  }, [dodaj]);

  const handleChange = (e) => {
    const { name, value } = e.target;

    if (name === "tema" && dataType === "predavače") {
      const selectedOptions = Array.from(e.target.selectedOptions).map(
        (option) => option.value
      );

      setFormData((prevFormData) => ({
        ...prevFormData,
        tema: [...prevFormData.tema, ...selectedOptions],
      }));
    } else if (name === "radionice" && dataType === "organizacije") {
      const selectedOptions = Array.from(e.target.selectedOptions).map(
        (option) => option.value
      );

      setFormData((prevFormData) => ({
        ...prevFormData,
        radionice: [...prevFormData.radionice, ...selectedOptions],
      }));
    } else if (e.target.type === "date") {
      const formattedDate = value.split("-").reverse().join(".");
      setFormData({ ...formData, [name]: formattedDate });
    } else {
      setFormData({ ...formData, [name]: value });
    }
  };

  const formatDateForInput = (date) => {
    if (!date) return "";
    const [day, month, year] = date.split(".");
    return `${year}-${month}-${day}`;
  };

  const handleSubmit = (e, callback) => {
    e.preventDefault();
    let url;
    let method;

    if (dodaj) {
      if (dataType === "organizacije") {
        url = `http://localhost:3001/organizacije`;
      } else if (dataType === "radionice") {
        url = `http://localhost:3001/radionice`;
      } else if (dataType === "predavače") {
        url = `http://localhost:3001/predavaci`;
      }
      method = "post";
    } else {
      if (dataType === "organizacije") {
        url = `http://localhost:3001/organizacije/${id}`;
      } else if (dataType === "radionice") {
        url = `http://localhost:3001/radionice/${id}`;
      } else if (dataType === "predavače") {
        url = `http://localhost:3001/predavaci/${id}`;
      }
      method = "put";
    }

    let dataToSend;
    if (dataType === "predavače") {
      dataToSend = {
        id: formData.id,
        ime: formData.ime,
        tema: formData.tema,
        slika: formData.slika,
        biografija: formData.biografija,
        organizacija: formData.organizacija,
      };
    } else if (dataType === "organizacije") {
      dataToSend = {
        id: formData.id,
        ime: formData.ime,
        opis: formData.opis,
        radionice: formData.radionice,
      };
    } else if (dataType === "radionice") {
      dataToSend = {
        id: formData.id,
        ime: formData.ime,
        datum: formData.datum,
        predavac: formData.predavac,
        opis: formData.opis,
        broj_prijava: formData.broj_prijava,
        tema: formData.tema,
        tezina: formData.tezina,
        slika: formData.slika,
      };
    }
    axios({
      method: method,
      url: url,
      data: dataToSend,
    })
      .then(() => {
        setFeedback("Data saved successfully!");
        setTimeout(() => {
          setFeedback("");
          setShowModal(false);
          callback();
        }, 1000);
      })
      .catch((error) => {
        console.error(`Error ${dodaj ? "adding" : "updating"} data:`, error);
      });
  };

  return (
    <div className="modalContainer">
      <form
        className="modalPodaci"
        onSubmit={(e) => handleSubmit(e, refreshData)}
      >
        <span
          className="close"
          onClick={() => {
            setShowModal(false);
          }}
        >
          &times;
        </span>
        <h2 style={{ paddingTop: "50px", marginBottom: "40px" }}>
          {dodaj ? <>Dodaj</> : <>Uredi {ime}</>}
        </h2>
        <div className="polja">
          <label htmlFor="ime" className="labelaModal">
            Ime:
          </label>
          <input
            type="text"
            id="ime"
            name="ime"
            required
            placeholder="Ime"
            value={formData.ime}
            onChange={handleChange}
          />
          {dataType === "radionice" && (
            <>
              <label htmlFor="datumOdrzavanja" className="labelaModal">
                Datum održavanja:
              </label>
              <input
                type="date"
                id="datumOdrzavanja"
                name="datum"
                required
                placeholder="Datum održavanja"
                value={formatDateForInput(formData.datum)}
                onChange={handleChange}
              />
              <label htmlFor="predavac" className="labelaModal">
                Predavač:
              </label>
              <select
                id="predavac"
                name="predavac"
                required
                value={formData.predavac}
                onChange={handleChange}
              >
                {predavaci.map((predavac) => (
                  <option key={predavac.id} value={predavac.ime}>
                    {predavac.ime}
                  </option>
                ))}
              </select>
            </>
          )}
          {dataType !== "predavače" && (
            <>
              <label htmlFor="opis" className="labelaModal">
                Opis:
              </label>
              <textarea
                id="opis"
                name="opis"
                required
                placeholder="Opis"
                value={formData.opis}
                onChange={handleChange}
                style={{
                  fontSize: "1rem",
                  fontFamily:
                    "Inter, system-ui, Avenir, Helvetica, Arial, sans-serif",
                }}
              />
            </>
          )}
          {dataType !== "organizacije" && (
            <>
              <label htmlFor="tema" className="labelaModal">
                Tema:
              </label>
              <select
                id="tema"
                name="tema"
                required
                value={formData.tema}
                onChange={handleChange}
                multiple={dataType === "predavače"}
              >
                {teme.map((tema) => (
                  <option key={tema.id} value={tema.ime}>
                    {tema.ime}
                  </option>
                ))}
              </select>
            </>
          )}
          {dataType === "radionice" && (
            <>
              <label htmlFor="tezina" className="labelaModal">
                Težina:
              </label>
              <select
                id="tezina"
                name="tezina"
                required
                value={formData.tezina}
                onChange={handleChange}
              >
                {tezine.map((tezina) => (
                  <option key={tezina.id} value={tezina.ime}>
                    {tezina.ime}
                  </option>
                ))}
              </select>
            </>
          )}
          {dataType !== "organizacije" && (
            <>
              <label htmlFor="slika" className="labelaModal">
                Slika:
              </label>
              <input
                type="text"
                id="slika"
                name="slika"
                required
                placeholder="Slika"
                value={formData.slika}
                onChange={handleChange}
              />
            </>
          )}
          {dataType === "predavače" && (
            <>
              <label htmlFor="biografija" className="labelaModal">
                Biografija:
              </label>
              <textarea
                id="biografija"
                name="biografija"
                required
                placeholder="Biografija"
                value={formData.biografija}
                onChange={handleChange}
                style={{
                  fontSize: "1rem",
                  fontFamily:
                    "Inter, system-ui, Avenir, Helvetica, Arial, sans-serif",
                }}
              />
              <label htmlFor="organizacija" className="labelaModal">
                Organizacija:
              </label>
              <input
                type="text"
                id="organizacija"
                name="organizacija"
                required
                placeholder="Organizacija"
                value={formData.organizacija}
                onChange={handleChange}
              />
            </>
          )}
          {dataType === "organizacije" && (
            <>
              <label htmlFor="radionice" className="labelaModal">
                Radionice:
              </label>
              <select
                id="radionice"
                name="radionice"
                required
                value={formData.radionice}
                onChange={handleChange}
                multiple
              >
                {" "}
                {radioniceSve.map((radionica) => (
                  <option key={radionica.id} value={radionica.id}>
                    {radionica.id}
                  </option>
                ))}
              </select>
            </>
          )}
          <button className="btn_primary" style={{ marginTop: "20px" }}>
            Spremi
          </button>
        </div>
        {feedback && (
          <p style={{ color: "green", fontWeight: "bold" }}>{feedback}</p>
        )}
      </form>
    </div>
  );
};
export default ModalData;
