import { useEffect } from "react";

const Toggle = ({ setToggle, toggle }) => {
  useEffect(() => {
    localStorage.setItem("toggle", JSON.stringify(toggle));
  }, [toggle]);

  const handleToggleChange = () => {
    setToggle(!toggle);
  };

  return (
    <div style={{ textAlign: "center" }}>
      <label className="switch">
        <input type="checkbox" onChange={handleToggleChange} checked={toggle} />
        <span className="slider round"></span>
        <br />
        <label>{toggle ? "Admin" : "User"}</label>
      </label>
    </div>
  );
};
export default Toggle;
