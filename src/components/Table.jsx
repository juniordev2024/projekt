import axios from "axios";
import useHandleUredi from "../utils/handleUredi";
import ModalData from "./ModalData";

const Table = ({
  podaci,
  selectedItem,
  setOrganizacije,
  setPredavaci,
  setRadionice,
  refreshData,
}) => {
  const { handleUredi, modalData, showModal, setShowModal } = useHandleUredi();

  const handleDelete = (id, selectedItem) => {
    const isConfirmed = window.confirm("Želite li izbrisati ovaj podatak?");

    if (isConfirmed) {
      if (selectedItem === "organizacije") {
        axios
          .delete(`http://localhost:3001/organizacije/${id}`)
          .then(() => {
            axios
              .get("http://localhost:3001/organizacije")
              .then((rez) => setOrganizacije(rez.data));
            refreshData();
          })
          .catch((error) => {
            console.error(`Error brisanja organizacije sa ID-em ${id}:`, error);
          });
      }
      if (selectedItem === "radionice") {
        axios
          .delete(`http://localhost:3001/radionice/${id}`)
          .then(() => {
            axios.get("http://localhost:3001/radionice").then((rez) => {
              setRadionice(rez.data);
              refreshData();
            });
          })
          .catch((error) => {
            console.error(`Error brisanja radionice sa ID-em ${id}:`, error);
          });
      }
      if (selectedItem === "predavače") {
        axios
          .delete(`http://localhost:3001/predavaci/${id}`)
          .then(() => {
            axios.get("http://localhost:3001/predavaci").then((rez) => {
              setPredavaci(rez.data);
              refreshData();
            });
          })
          .catch((error) => {
            console.error(`Error brisanja predavača sa ID-em ${id}:`, error);
          });
      }
    }
  };

  const handleEditClick = (id) => {
    handleUredi(id, selectedItem);
  };

  return (
    <>
      <table className="custom-table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Ime</th>
            {selectedItem === "radionice" && <th>Broj prijava</th>}
            {selectedItem === "organizacije" && <th>Radionice</th>}
            {selectedItem === "predavače" && <th>Organizacija</th>}
            <th>Admin akcija</th>
          </tr>
        </thead>
        <tbody>
          {podaci.map((p) => {
            return (
              <tr key={p.id}>
                <td>{p.id}</td>
                <td>{p.ime}</td>
                {selectedItem === "radionice" && <td>{p.broj_prijava}</td>}
                {selectedItem === "organizacije" && (
                  <td>
                    {p.radionice.length > 1 ? (
                      <>{p.radionice[0]}...</>
                    ) : (
                      <>{p.radionice}</>
                    )}
                  </td>
                )}
                {selectedItem === "predavače" && <td>{p.organizacija}</td>}
                <td>
                  <button
                    className="btn_admin"
                    style={{ margin: "0 15px 0 15px", padding: "0.5rem 1rem" }}
                    onClick={() => handleEditClick(p.id)}
                  >
                    Uredi
                  </button>
                  <button
                    className="btn_admin"
                    style={{
                      margin: "0 15px 0 15px",
                      padding: "0.5rem 1rem",
                      background: "#fd0808cc",
                    }}
                    onClick={() => handleDelete(p.id, selectedItem)}
                  >
                    Obriši
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      {modalData && showModal && (
        <ModalData
          setShowModal={setShowModal}
          id={modalData.id}
          ime={modalData.ime}
          datum={modalData.datum}
          predavac={modalData.predavac}
          opis={modalData.opis}
          tema={modalData.tema}
          tezina={modalData.tezina}
          slika={modalData.slika}
          broj_prijava={modalData.broj_prijava}
          biografija={modalData.biografija}
          organizacija={modalData.organizacija}
          dataType={selectedItem}
          radionice={modalData.radionice}
          refreshData={refreshData}
        />
      )}
    </>
  );
};
export default Table;
