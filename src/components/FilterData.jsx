import { useEffect } from "react";
import axios from "axios";

const FilterData = ({
  data,
  imeFiltera,
  name,
  setRadionice,
  filter,
  setFilter,
  filterValue,
  setLoading,
  filterRadionica,
  filterPredavaca,
  setPredavaci,
  imePredavaca,
}) => {
  function promjenaPolja(e) {
    const { name, value } = e.target;
    setFilter({ ...filter, [name]: value });
  }
  useEffect(() => {
    //dohvat podataka prebrz pa je stavljen timeout za prikaz loading state-a
    setLoading(true);

    const hasFilters = Object.values(filter).some((value) => value !== "");

    if (filterRadionica) {
      if (hasFilters) {
        let url = "http://localhost:3001/radionice?";
        if (imePredavaca) {
          const params = new URLSearchParams(imePredavaca).toString();
          url += `${params}&`;
        }
        url += `tema=${filter.tema}&tezina=${filter.tezina}`;

        axios.get(url).then((res) => {
          setTimeout(() => {
            setLoading(false);
          }, 1000);
          setRadionice(res.data);
        });
      }
    }
    if (filterPredavaca) {
      axios
        .get(`http://localhost:3001/predavaci`)
        .then((res) => {
          setTimeout(() => {
            setLoading(false);
          }, 1000);
          let filteredData = res.data;

          if (filter.organizacija && filter.tema) {
            filteredData = filteredData.filter(
              (predavac) =>
                predavac.organizacija === filter.organizacija &&
                predavac.tema.includes(filter.tema)
            );
          } else if (filter.organizacija) {
            filteredData = filteredData.filter(
              (predavac) => predavac.organizacija === filter.organizacija
            );
          } else if (filter.tema) {
            filteredData = filteredData.filter((predavac) =>
              predavac.tema.includes(filter.tema)
            );
          }

          setPredavaci(filteredData);
        })
        .catch((error) => {
          console.error("Error fetching data:", error);
        });
    }
    setLoading(false);
  }, [filter]);

  return (
    <div className="filter" style={{ marginBottom: "40px" }}>
      <h3 style={{ color: "black" }}>{imeFiltera}:</h3>
      <div style={{ display: "grid" }}>
        {data.map((d) => {
          return (
            <label key={d.id}>
              <input
                type="radio"
                name={name}
                value={d.ime}
                required
                onChange={promjenaPolja}
                checked={filterValue === d.ime}
              />
              {d.ime}
            </label>
          );
        })}
      </div>
    </div>
  );
};

export default FilterData;
