import { useEffect, useState } from "react";
import axios from "axios";
import FilterData from "./FilterData";

const Filter = ({
  setRadionice,
  filterRadionica,
  filterPredavaca,
  setPredavaci,
  imePredavaca,
}) => {
  const [teme, setTeme] = useState([]);
  const [tezine, setTezine] = useState([]);
  const [organizacije, setOrganizacije] = useState([]);
  const [filter, setFilter] = useState({
    tema: "",
    tezina: "",
    organizacija: "",
  });
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    axios.get("http://localhost:3001/teme").then((res) => setTeme(res.data));

    axios
      .get("http://localhost:3001/tezine")
      .then((res) => setTezine(res.data));

    axios
      .get("http://localhost:3001/organizacije")
      .then((res) => setOrganizacije(res.data));
  }, []);

  return (
    <div style={{ marginTop: "60px" }}>
      {filterRadionica && (
        <>
          <FilterData
            data={tezine}
            name="tezina"
            imeFiltera="Težina"
            setRadionice={setRadionice}
            filter={filter}
            filterValue={filter.tezina}
            setFilter={setFilter}
            setLoading={setLoading}
            filterRadionica={filterRadionica}
            imePredavaca={imePredavaca}
          />
          <FilterData
            data={teme}
            name="tema"
            imeFiltera="Teme"
            setRadionice={setRadionice}
            filter={filter}
            filterValue={filter.tema}
            setFilter={setFilter}
            setLoading={setLoading}
            filterRadionica={filterRadionica}
            imePredavaca={imePredavaca}
          />
        </>
      )}
      {filterPredavaca && (
        <>
          <FilterData
            data={organizacije}
            name="organizacija"
            imeFiltera="Organizacije"
            setPredavaci={setPredavaci}
            filter={filter}
            filterValue={filter.organizacija}
            setFilter={setFilter}
            setLoading={setLoading}
            filterPredavaca={filterPredavaca}
          />
          <FilterData
            data={teme}
            name="tema"
            imeFiltera="Teme"
            setPredavaci={setPredavaci}
            filter={filter}
            filterValue={filter.tema}
            setFilter={setFilter}
            setLoading={setLoading}
            filterPredavaca={filterPredavaca}
          />
        </>
      )}
      {loading && <h2 style={{ color: "#fb6e27" }}>Učitavanje..</h2>}
    </div>
  );
};
export default Filter;
