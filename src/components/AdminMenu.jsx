import { useState } from "react";
import ModalData from "./ModalData";

const AdminMenu = ({ selectedItem, setSelectedItem, refreshData }) => {
  const [clickedItem, setClickedItem] = useState("radionice");
  const [showModal, setShowModal] = useState(false);

  const handleItemClick = (itemName) => {
    setSelectedItem(itemName);
    setClickedItem(itemName);
  };

  const handleAddClick = () => {
    setShowModal(true);
  };

  return (
    <div className="below-nav">
      <ul className="sub-menu">
        <li>
          <a
            className={clickedItem === "radionice" ? "active" : ""}
            onClick={() => handleItemClick("radionice")}
          >
            Radionice
          </a>
        </li>
        <li>
          <a
            className={clickedItem === "organizacije" ? "active" : ""}
            onClick={() => handleItemClick("organizacije")}
          >
            Organizacije
          </a>
        </li>
        <li>
          <a
            className={clickedItem === "predavače" ? "active" : ""}
            onClick={() => handleItemClick("predavače")}
          >
            Predavači
          </a>
        </li>
        <li>
          <button
            className="btn_admin"
            style={{ margin: 0, padding: " 0.4rem 1rem" }}
            onClick={handleAddClick}
          >
            {selectedItem ? `+Dodaj ${selectedItem}` : "+Dodaj"}
          </button>
        </li>
      </ul>
      {showModal && (
        <ModalData
          dataType={selectedItem}
          setShowModal={setShowModal}
          dodaj={true}
          refreshData={refreshData}
        />
      )}
    </div>
  );
};
export default AdminMenu;
