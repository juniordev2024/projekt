import { NavLink } from "react-router-dom";
import Toggle from "./Toggle";

const NavBar = ({ toggle, setToggle }) => {
  return (
    <>
      <nav className="nav">
        <NavLink to="/" className="site-title">
          EDIT Code School
        </NavLink>
        <ul>
          <li>
            <NavLink
              to="/"
              className={({ isActive }) => (isActive ? "active" : "")}
            >
              Radionice
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/predavaci"
              className={({ isActive }) => (isActive ? "active" : "")}
            >
              Predavači
            </NavLink>
          </li>
          {toggle && (
            <li>
              <NavLink
                to="/administracija"
                className={({ isActive }) => (isActive ? "active" : "")}
              >
                Administracija
              </NavLink>
            </li>
          )}
        </ul>
        <Toggle toggle={toggle} setToggle={setToggle} />
      </nav>
    </>
  );
};
export default NavBar;
