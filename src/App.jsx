import "./App.css";
import NavBar from "./components/NavBar";
import { Route, Routes } from "react-router-dom";
import Administracija from "./pages/Administracija";
import Predavaci from "./pages/Predavaci";
import Radionice from "./pages/Radionice";
import RadionicePredavaca from "./pages/RadionicePredavaca";
import { useState, useEffect } from "react";

function App() {
  const [toggle, setToggle] = useState(() => {
    const storedToggle = localStorage.getItem("toggle");
    return storedToggle ? JSON.parse(storedToggle) : false;
  });

  useEffect(() => {
    localStorage.setItem("toggle", JSON.stringify(toggle));
  }, [toggle]);

  return (
    <>
      <NavBar toggle={toggle} setToggle={setToggle} />
      <div className="container">
        <Routes>
          <Route
            path="/"
            element={<Radionice toggle={toggle} selectedItem="radionice" />}
          />
          <Route
            path="/predavaci"
            element={<Predavaci toggle={toggle} selectedItem="predavače" />}
          />
          <Route
            path="/administracija"
            element={<Administracija toggle={toggle} />}
          />
          <Route
            path="/predavaci/:ime_predavaca"
            element={<RadionicePredavaca toggle={toggle} />}
          />
        </Routes>
      </div>
    </>
  );
}

export default App;
