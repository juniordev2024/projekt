import { useState, useEffect } from "react";
import axios from "axios";
import Filter from "../components/Filter";
import { useNavigate } from "react-router-dom";
import useHandleUredi from "../utils/handleUredi";
import ModalData from "../components/ModalData";

const Predavaci = ({ toggle, selectedItem }) => {
  const [predavaci, setPredavaci] = useState([]);
  const navigate = useNavigate();
  const { handleUredi, modalData, showModal, setShowModal, setModalData } =
    useHandleUredi();
  const [isDodaj, setIsDodaj] = useState(true);

  useEffect(() => {
    axios
      .get("http://localhost:3001/predavaci")
      .then((res) => setPredavaci(res.data))
      .catch((error) => console.error("Error fetching predavaci:", error));
  }, []);

  const handlePregledajRadionice = (ime) => {
    const encodedIme = encodeURIComponent(ime);
    navigate(`/predavaci/${encodedIme}`);
  };

  const handleEditClick = (id) => {
    handleUredi(id, selectedItem);
    setIsDodaj(false);
  };

  const handleAddClick = () => {
    setShowModal(true);
    setModalData({
      id: "",
      ime: "",
      datum: "",
      predavac: "",
      opis: "",
      tema: [],
      tezina: "",
      slika: "",
      broj_prijava: 0,
    });
    setIsDodaj(true);
  };

  const refreshData = () => {
    axios
      .get(`http://localhost:3001/predavaci`)
      .then((res) => {
        setPredavaci(res.data);
      })
      .catch((error) => {
        console.error(`Error fetching data predavaci:`, error);
      });
  };

  return (
    <>
      <div>
        {toggle && (
          <div>
            <button className="btn_admin" onClick={handleAddClick}>
              +Dodaj novog predavača
            </button>
          </div>
        )}

        {predavaci.length === 0 ? (
          <h3
            className="nemaPodataka"
            style={{
              marginTop: "60px",
              marginLeft: "60px",
            }}
          >
            Nema podataka za prikaz
          </h3>
        ) : (
          predavaci.map((p) => {
            return (
              <div className="postPredavac" key={p.id}>
                <div style={{ textAlign: "center" }}>
                  <img
                    src={p.slika}
                    alt="random image"
                    style={{ maxWidth: "100%", maxHeight: "100%" }}
                  />
                </div>
                <div style={{ marginLeft: 0, textAlign: "center" }}>
                  <h1>{p.ime}</h1>
                  <p style={{ margin: "5px 5px 15px 5px" }}>{p.biografija}</p>
                  <br />
                  <h3>
                    Teme:{" "}
                    {p.tema.map((t, index) => {
                      return index === p.tema.length - 1 && p.tema
                        ? t
                        : t + ",";
                    })}
                  </h3>
                  <h3 style={{ marginBottom: "25px" }}>
                    Organizacija: {p.organizacija}
                  </h3>
                  <button
                    className="btn_primary"
                    onClick={() => handlePregledajRadionice(p.ime)}
                  >
                    Pregledaj radionice
                  </button>
                  {toggle && (
                    <button
                      className="btn_admin"
                      onClick={() => handleEditClick(p.id)}
                    >
                      Uredi
                    </button>
                  )}
                </div>
              </div>
            );
          })
        )}
      </div>
      {modalData && showModal && (
        <ModalData
          setShowModal={setShowModal}
          id={modalData.id}
          ime={modalData.ime}
          biografija={modalData.biografija}
          organizacija={modalData.organizacija}
          tema={modalData.tema}
          slika={modalData.slika}
          dataType={selectedItem}
          dodaj={isDodaj}
          refreshData={refreshData}
        />
      )}
      <Filter filterPredavaca={true} setPredavaci={setPredavaci} />
    </>
  );
};
export default Predavaci;
