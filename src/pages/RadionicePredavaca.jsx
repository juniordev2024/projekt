import { useParams } from "react-router-dom";
import Radionice from "./Radionice";

const RadionicePredavaca = ({ toggle }) => {
  const { ime_predavaca } = useParams();
  const filterPredavaca = { predavac: ime_predavaca };

  return (
    <>
      <h2
        className="header"
        style={{
          marginTop: "30px",
          marginLeft: "60px",
          textDecoration: "underline",
        }}
      >
        Radionice predavača: {ime_predavaca}
      </h2>
      <div style={{ display: "flex" }}>
        <Radionice toggle={toggle} filterPredavaca={filterPredavaca} />
      </div>
    </>
  );
};

export default RadionicePredavaca;
