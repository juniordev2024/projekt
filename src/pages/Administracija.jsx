import AdminMenu from "./../components/AdminMenu";
import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import Table from "./../components/Table";
import axios from "axios";

const Administracija = ({ toggle }) => {
  const navigate = useNavigate();
  const [selectedItem, setSelectedItem] = useState("radionice");
  const [predavaci, setPredavaci] = useState([]);
  const [radionice, setRadionice] = useState([]);
  const [organizacije, setOrganizacije] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:3001/predavaci")
      .then((res) => setPredavaci(res.data))
      .catch((error) =>
        console.error("Error fetching data from predavaci:", error)
      );

    axios
      .get("http://localhost:3001/radionice")
      .then((res) => setRadionice(res.data))
      .catch((error) =>
        console.error("Error fetching data from radionice:", error)
      );

    axios
      .get("http://localhost:3001/organizacije")
      .then((res) => setOrganizacije(res.data))
      .catch((error) =>
        console.error("Error fetching data from organizacije:", error)
      );
  }, []);

  const refreshData = () => {
    let endpoint;
    switch (selectedItem) {
      case "organizacije":
        endpoint = "organizacije";
        break;
      case "radionice":
        endpoint = "radionice";
        break;
      case "predavače":
        endpoint = "predavaci";
        break;
      default:
        return;
    }

    axios
      .get(`http://localhost:3001/${endpoint}`)
      .then((res) => {
        switch (selectedItem) {
          case "organizacije":
            setOrganizacije(res.data);
            break;
          case "radionice":
            setRadionice(res.data);
            break;
          case "predavače":
            setPredavaci(res.data);
            break;
          default:
            break;
        }
      })
      .catch((error) => {
        console.error(`Error fetching data for ${selectedItem}:`, error);
      });
  };

  return (
    <>
      {toggle ? (
        <>
          <AdminMenu
            selectedItem={selectedItem}
            setSelectedItem={setSelectedItem}
            refreshData={refreshData}
          />
          <div
            style={{
              marginTop: "60px",
              gridColumn: " 1 / -1",
              marginLeft: "100px",
              marginRight: "100px",
            }}
          >
            {selectedItem === "radionice" && (
              <Table
                podaci={radionice}
                selectedItem={selectedItem}
                setRadionice={setRadionice}
                refreshData={refreshData}
              />
            )}
            {selectedItem === "organizacije" && (
              <Table
                podaci={organizacije}
                selectedItem={selectedItem}
                setOrganizacije={setOrganizacije}
                refreshData={refreshData}
              />
            )}
            {selectedItem === "predavače" && (
              <Table
                podaci={predavaci}
                selectedItem={selectedItem}
                setPredavaci={setPredavaci}
                refreshData={refreshData}
              />
            )}
          </div>
        </>
      ) : (
        navigate("/")
      )}
    </>
  );
};
export default Administracija;
