import { useEffect, useState } from "react";
import Filter from "./../components/Filter";
import axios from "axios";
import Modal from "../components/Modal";
import ModalData from "../components/ModalData";
import useHandleUredi from "../utils/handleUredi";

const Radionice = ({ toggle, filterPredavaca, selectedItem }) => {
  const { handleUredi, modalData, showModal, setShowModal, setModalData } =
    useHandleUredi();
  const [isDodaj, setIsDodaj] = useState(true);

  const [radionice, setRadionice] = useState([]);
  const [modal, setModal] = useState(false);
  const [imeRadionice, setImeRadionice] = useState("");
  const [idRadionice, setIdRadionice] = useState("");
  const [brojPrijava, setBrojPrijava] = useState(0);

  useEffect(() => {
    const params = new URLSearchParams(filterPredavaca).toString();
    const url = `http://localhost:3001/radionice?${params}`;

    axios
      .get(url)
      .then((res) => {
        setRadionice(res.data);
      })
      .catch((error) => console.error("Error fetching radionice:", error));
  }, [filterPredavaca]);

  function handleClick(radionica) {
    setModal(true);
    setImeRadionice(radionica.ime);
    setIdRadionice(radionica.id);
    setBrojPrijava(radionica.broj_prijava);
  }

  const handleEditClick = (id) => {
    handleUredi(id, selectedItem);
    setIsDodaj(false);
  };

  const handleAddClick = () => {
    setShowModal(true);
    setModalData({
      id: "",
      ime: "",
      datum: "",
      predavac: "",
      opis: "",
      tema: [],
      tezina: "",
      slika: "",
      broj_prijava: 0,
    });
    setIsDodaj(true);
  };

  const refreshData = () => {
    axios
      .get(`http://localhost:3001/radionice`)
      .then((res) => {
        setRadionice(res.data);
      })
      .catch((error) => {
        console.error(`Error fetching data radionice:`, error);
      });
  };

  return (
    <>
      <div style={{ flex: 1 }}>
        {toggle && (
          <div>
            <button className="btn_admin" onClick={handleAddClick}>
              +Dodaj novu radionicu
            </button>
          </div>
        )}

        {radionice.length === 0 ? (
          <h3
            className="nemaPodataka"
            style={{
              marginTop: "60px",
              marginLeft: "60px",
            }}
          >
            Nema podataka za prikaz
          </h3>
        ) : (
          radionice.map((r) => {
            return (
              <div className="post" key={r.id}>
                <div>
                  <img src={r.slika} alt="random image" />
                </div>
                <div style={{ marginLeft: "35px" }}>
                  <h3>Predavači: {r.predavac}</h3>
                  <h3>Datum održavanja: {r.datum}</h3>
                  <br />
                  <h1>{r.ime}</h1>
                  <p>{r.opis}</p>
                  <button
                    className="btn_primary"
                    onClick={() => handleClick(r)}
                  >
                    Prijavi se
                  </button>
                  {toggle && (
                    <button
                      className="btn_admin"
                      onClick={() => handleEditClick(r.id)}
                    >
                      Uredi
                    </button>
                  )}
                </div>
              </div>
            );
          })
        )}
      </div>

      <Filter
        setRadionice={setRadionice}
        filterRadionica={true}
        imePredavaca={filterPredavaca}
      />

      {modal && (
        <Modal
          imeRadionice={imeRadionice}
          setModal={setModal}
          idRadionice={idRadionice}
          brojPrijava={brojPrijava}
        />
      )}
      {modalData && showModal && (
        <ModalData
          setShowModal={setShowModal}
          id={modalData.id}
          ime={modalData.ime}
          datum={modalData.datum}
          predavac={modalData.predavac}
          opis={modalData.opis}
          tema={modalData.tema}
          tezina={modalData.tezina}
          slika={modalData.slika}
          broj_prijava={modalData.broj_prijava}
          dataType={selectedItem}
          refreshData={refreshData}
          dodaj={isDodaj}
        />
      )}
    </>
  );
};
export default Radionice;
