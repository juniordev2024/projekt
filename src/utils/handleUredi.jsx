import { useState } from "react";
import axios from "axios";

const useHandleUredi = () => {
  const [modalData, setModalData] = useState(null);
  const [showModal, setShowModal] = useState(false);

  const handleUredi = (id, selectedItem) => {
    let url;
    if (selectedItem === "organizacije") {
      url = `http://localhost:3001/organizacije/${id}`;
    } else if (selectedItem === "radionice") {
      url = `http://localhost:3001/radionice/${id}`;
    } else if (selectedItem === "predavače") {
      url = `http://localhost:3001/predavaci/${id}`;
    }

    axios
      .get(url)
      .then((response) => {
        setModalData(response.data);
        setShowModal(true);
      })
      .catch((error) => {
        console.error(
          `Error fetching data for ${selectedItem} with ID ${id}:`,
          error
        );
      });
  };

  return { handleUredi, modalData, showModal, setShowModal, setModalData };
};

export default useHandleUredi;
